package br.com.cartoes.creditcard.models.dto;

import br.com.cartoes.creditcard.models.CreditCard;
import br.com.cartoes.customer.models.Customer;
import br.com.cartoes.customer.models.dto.CreateCustomerRequest;
import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper {

    public CreditCard toCreditCard(CreateCreditCardRequest createCreditCardRequest) {
        Customer customer = new Customer();
        customer.setId(createCreditCardRequest.getCustomerId());

        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(createCreditCardRequest.getNumber());
        creditCard.setCustomer(customer);

        return creditCard;
    }

    public CreditCard toCreditCard(UpdateCreditCardRequest updateCreditCardRequest) {
        CreditCard creditCard = new CreditCard();
        creditCard.setActive(updateCreditCardRequest.getActive());
        return creditCard;
    }

    public CreateCreditCardResponse toCreateCreditCardResponse(CreditCard creditCard) {
        CreateCreditCardResponse createCreditCardResponse = new CreateCreditCardResponse();
        createCreditCardResponse.setId(creditCard.getId());
        createCreditCardResponse.setNumber(creditCard.getNumber());
        createCreditCardResponse.setActive(creditCard.getActive());
        createCreditCardResponse.setCustomerId(creditCard.getCustomer().getId());
        return createCreditCardResponse;
    }

    public GetCreditCardResponse toGetCreditCardResponse(CreditCard creditCard) {
        GetCreditCardResponse getCreditCardResponse = new GetCreditCardResponse();
        getCreditCardResponse.setId(creditCard.getId());
        getCreditCardResponse.setNumber(creditCard.getNumber());
        getCreditCardResponse.setCustomerId(creditCard.getCustomer().getId());
        return getCreditCardResponse;
    }

    public br.com.cartoes.creditcard.models.dto.UpdateCreditCardResponse toUpdateCreditCardResponse(CreditCard creditCard) {
        br.com.cartoes.creditcard.models.dto.UpdateCreditCardResponse updateCreditCardResponse = new br.com.cartoes.creditcard.models.dto.UpdateCreditCardResponse();
        updateCreditCardResponse.setId(creditCard.getId());
        updateCreditCardResponse.setNumber(creditCard.getNumber());
        updateCreditCardResponse.setActive(creditCard.getActive());
        updateCreditCardResponse.setCustomerId(creditCard.getCustomer().getId());
        return updateCreditCardResponse;
    }
}
