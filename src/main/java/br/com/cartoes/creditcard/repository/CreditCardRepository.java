package br.com.cartoes.creditcard.repository;

import br.com.cartoes.creditcard.models.CreditCard;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
}
