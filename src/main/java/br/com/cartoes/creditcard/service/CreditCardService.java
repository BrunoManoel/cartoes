package br.com.cartoes.creditcard.service;

import br.com.cartoes.creditcard.exception.CreditCardNotFoundException;
import br.com.cartoes.creditcard.models.CreditCard;
import br.com.cartoes.creditcard.repository.CreditCardRepository;
import br.com.cartoes.customer.models.Customer;
import br.com.cartoes.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CreditCardRepository creditCardRepository;

    public CreditCard create(CreditCard creditCard) {
        Customer customer = customerService.getById(creditCard.getCustomer().getId());
        creditCard.setCustomer(customer);

        creditCard.setActive(false);

        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard updatedCreditCard) {
        CreditCard creditCard = getById(updatedCreditCard.getId());

        creditCard.setActive(updatedCreditCard.getActive());

        return creditCardRepository.save(creditCard);
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }
}
