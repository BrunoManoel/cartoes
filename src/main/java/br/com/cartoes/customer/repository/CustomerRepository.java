package br.com.cartoes.customer.repository;

import br.com.cartoes.customer.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
